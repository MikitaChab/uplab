﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WIA;

namespace Skaner
{
    public partial class Form1 : Form
    {
        bool isOpen = true;
        String path;
        Controller controller;
        int format = 0;
        public Form1()
        {
            InitializeComponent();
            controller = new Controller();

     
        }

  
        private void scannersBtn_Click(object sender, EventArgs e)
        {
            cbScannerList.Items.Clear();
            controller.getScanners();
            for (int i = 0; i < controller.scanners.Count; i++)
            {
                cbScannerList.Items.Add(controller.scanners.ToArray()[i].Properties["Name"].get_Value());
            }
            cbScannerList.SelectedIndex = 0;
        }

        private void scannersList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbScannerList.Items.Count < 0)
            {
                controller.chosenDevice = controller.scanners[cbScannerList.SelectedIndex];
                controller.ConnectScannerItem();
            }
        }

        private void scanBtn_Click(object sender, EventArgs e)
        {
            if (path == null)
            {
                openFolderDialog();
            }

            if (controller.scanners.Count == 0)
            {
                controller.getScanners();
            }

            if (controller.scanners.Count > 0)
            {
                if (cbScannerList.SelectedIndex != -1)
                {
                    controller.chosenDevice = controller.scanners[cbScannerList.SelectedIndex];
                }
                updateSettings();
                controller.scan(FormatID.wiaFormatJPEG);
                showImg();
            }
        }

        private void folderBtn_Click(object sender, EventArgs e)
        {
            openFolderDialog();
        }

        private void imgShow_Click(object sender, EventArgs e)
        {
            showImg();
        }



        private void dialogBtn_Click(object sender, EventArgs e)
        {
            if(path == null)
            {
                openFolderDialog();
            }
            updateSettings();
            controller.scanDialog(format);
            showImg();
        }

        private void openFolderDialog()
        {
            FolderBrowserDialog folder = new FolderBrowserDialog();
            if (folder.ShowDialog() == DialogResult.OK)
            {
                string filename;
                switch (format)
                {
                    case 0:
                        filename = "\\scan.png";
                        break;
                    default:
                    case 1:
                        filename = "\\scan.jpeg";
                        break;
                    case 2:
                        filename = "\\scan.bmp";
                        break;
                }
                path = folder.SelectedPath + filename;
                Console.WriteLine(path);
                controller.filePath = path;
            }
        }
        private void showImg()
        {
            if (path == null)
            {
                openFolderDialog();
            }
            pictureBox1.ImageLocation = controller.filePath;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
        }



        private void verticalResolutionText_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {

                e.Handled = true;
            }



        }
        private void updateSettings()
        {

        }

        private void horizontalResolutionText_KeyPressed(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
            {

                e.Handled = true;
            }



        }

        private void settingsBtn_Click(object sender, EventArgs e)
        {

        }

        private void cbScannerList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void colorCB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(colorCB.SelectedIndex == 0)
            {
                format = 0;
            }
            else if (colorCB.SelectedIndex == 1)
            {
                format = 1;
            }
            else if (colorCB.SelectedIndex == 2)
            {
                format = 2;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            isOpen = false;
        }
    }
}
