#include "Bluetooth.h"


int main()
{
    Bluetooth bt;
    bt.FindAdapters(true);
    bt.Look_For_Devices(true);
    int tmp;
    while(bt.selected_device == NULL)
    {
        std::cout << "Select Device: "; std::cin >> tmp; std::cout << std::endl;
        bt.select_device(tmp);
		if (tmp == 99)
		{
			bt.Look_For_Devices(true);
			continue;
		}
        if(bt.selected_device != NULL)
        {
            std::cout << "Wybrano urzadzenie: ";
            bt.show_device_details(bt.selected_device);
            break;		
        }
        else
            std::cout << "Podano zly nr urzadzenia. Try Again. " << std::endl;

    }
    std::cout << "Autoryzowanie urzadzenia... ";
    if(!bt.authenticate_selected_device())
    {
        std::cout << "Nie mozna bylo dokonac autoryzacji urzadzenia!" << std::endl;
        return 0;
    }
	
    std::cout << "OK!" << std::endl;
    std::cout << "Wysylanie pliku... ";
    bt.send_file("1.txt");
	system("PAUSE");
    return 0;
}