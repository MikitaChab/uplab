﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;


namespace GPSReader
{
    public partial class frmPP : Form
    {
        public string Latitude;
        public string Longitude;

        public frmPP()
        {
            InitializeComponent();
            timer1.Enabled = true;
            timer1.Start();
            btnMapIt.Enabled = false;
            textBox1.Text = "COM3";

            // Try to open the serial port
            try
            {
                serialPort1.PortName = "COM3";
                serialPort1.BaudRate = 4800;
                serialPort1.DataBits = 8;
                serialPort1.Parity = Parity.None;
                serialPort1.StopBits = StopBits.One;
                serialPort1.Open();
                button1.Text = "Stop Updates";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                timer1.Enabled = false;
                button1.Text = "Update";
                return;
            }
        }

        /// <span class="code-SummaryComment"><summary></span>
        /// Try to update present position if the port is setup correctly
        /// and the GPS device is returning values
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="sender"></param></span>
        /// <span class="code-SummaryComment"><param name="e"></param></span>
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (serialPort1.IsOpen)
            {
                labelPortStatus.Text = "COM Port Open";
                string data =  serialPort1.ReadExisting();
               string[] strArr = data.Split('$');
                for (int i = 0; i < strArr.Length; i++)
                {
                    string strTemp = "GPGGA,123519,4807.038,N,01131.000,E,1,08,0.9,545.4,M,46.9,M,,*47";//strArr[i];
                    textBoxMessege.Text = strTemp;
                    string[] lineArr = strTemp.Split(',');
                    if (lineArr[0] == "GPGGA")
                    {
                        try
                        {
                            //Display Latitude
                            string lat = lineArr[2].Replace('.',',');
                            Latitude = lineArr[3].ToString() + parseGeo(Convert.ToDouble(lat));
                            //Display Longitude
                            string lon = lineArr[4].Replace('.', ',');
                            Longitude = lineArr[5].ToString() + parseGeo(Convert.ToDouble(lon));

                            txtLat.Text = Latitude;
                            txtLong.Text = Longitude;

                            btnMapIt.Enabled = true;
                            //dispay number of satelites conected
                            textBoxNumerSatelites.Text = lineArr[7];
                            //display time
                            string timeStr = lineArr[1];
                            string displayTimeStr = timeStr.Substring(0, 2) + ":" + timeStr.Substring(2, 2) + ":" + timeStr.Substring(4, 2);
                            textBoxTime.Text = displayTimeStr;
                        }
                        catch(Exception ex)
                        {  
                            //Cannot Read GPS values
                            timer1.Enabled = false;
                            MessageBox.Show(ex.Message);
                            txtLat.Text = "GPS Unavailable";
                            txtLong.Text = "GPS Unavailable";
                            btnMapIt.Enabled = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                // txtLat.Text = "COM Port Closed";
                //txtLong.Text = "COM Port Closed";
                labelPortStatus.Text = "COM Port Closed";
                btnMapIt.Enabled = false;
            }
        }

        private string parseGeo(double value) {
            string result = null;
            double degValue = value / 100;
            int degrees = (int)degValue;
            double decMinutesSeconds = ((degValue - degrees)) / .60;
            double minuteValue = decMinutesSeconds * 60;
            int minutes = (int)minuteValue;
            double secsValue = (minuteValue - minutes) * 60;
            result = degrees + "\u00B0" + " " + minutes + "' " + String.Format("{0:F1}", secsValue).Replace(',','.') +'"';
            return result;
        }

        /// <span class="code-SummaryComment"><summary></span>
        /// Enable or disable the timer to start continuous
        /// updates or disable all updates
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="sender"></param></span>
        /// <span class="code-SummaryComment"><param name="e"></param></span>
        private void button1_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled == true)
            {
                timer1.Enabled = false;
            }
            else
            {
                timer1.Enabled = true;
            }

            if (button1.Text == "Update")
            {
                button1.Text = "Stop Updates";
                txtLat.Text = "";
                txtLong.Text = "";
            }
            else
            {
                button1.Text = "Update";
            }
        }

        /// <span class="code-SummaryComment"><summary></span>
        /// Exit the application
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="sender"></param></span>
        /// <span class="code-SummaryComment"><param name="e"></param></span>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
        /// <span class="code-SummaryComment"><summary></span>
        /// Open a map of the present position
        /// <span class="code-SummaryComment"></summary></span>
        /// <span class="code-SummaryComment"><param name="sender"></param></span>
        /// <span class="code-SummaryComment"><param name="e"></param></span>
        private void btnMapIt_Click(object sender, EventArgs e)
        { 
            try
            {
                StringBuilder queryAddress = new StringBuilder();
                queryAddress.Append("http://maps.google.com/maps?q=");

                if (Latitude != string.Empty)
                {
                    queryAddress.Append(Latitude);
                }

                if (Longitude != string.Empty)
                {
                    queryAddress.Append(Longitude);
                }

                System.Diagnostics.Process.Start(queryAddress.ToString().Replace(" ",""));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort1.Close();
                serialPort1.PortName = textBox1.Text;
                serialPort1.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                timer1.Enabled = false;
                button1.Text = "Update";
                return;
            }
        }   
    }
}
