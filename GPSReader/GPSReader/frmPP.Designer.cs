﻿namespace GPSReader
{
    partial class frmPP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.txtLong = new System.Windows.Forms.TextBox();
            this.txtLat = new System.Windows.Forms.TextBox();
            this.btnMapIt = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button_Port = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxMessege = new System.Windows.Forms.TextBox();
            this.labelMessege = new System.Windows.Forms.Label();
            this.labelNumberSatelites = new System.Windows.Forms.Label();
            this.textBoxNumerSatelites = new System.Windows.Forms.TextBox();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.labelTime = new System.Windows.Forms.Label();
            this.labelUTC = new System.Windows.Forms.Label();
            this.labelPortStatus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(199, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtLong
            // 
            this.txtLong.Location = new System.Drawing.Point(72, 228);
            this.txtLong.Name = "txtLong";
            this.txtLong.Size = new System.Drawing.Size(99, 20);
            this.txtLong.TabIndex = 1;
            // 
            // txtLat
            // 
            this.txtLat.Location = new System.Drawing.Point(71, 253);
            this.txtLat.Name = "txtLat";
            this.txtLat.Size = new System.Drawing.Size(100, 20);
            this.txtLat.TabIndex = 2;
            // 
            // btnMapIt
            // 
            this.btnMapIt.Location = new System.Drawing.Point(72, 279);
            this.btnMapIt.Name = "btnMapIt";
            this.btnMapIt.Size = new System.Drawing.Size(85, 23);
            this.btnMapIt.TabIndex = 3;
            this.btnMapIt.Text = "Google Map";
            this.btnMapIt.UseVisualStyleBackColor = true;
            this.btnMapIt.Click += new System.EventHandler(this.btnMapIt_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 256);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Latitude";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Longitude";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(71, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 6;
            // 
            // button_Port
            // 
            this.button_Port.Location = new System.Drawing.Point(177, 6);
            this.button_Port.Name = "button_Port";
            this.button_Port.Size = new System.Drawing.Size(107, 20);
            this.button_Port.TabIndex = 7;
            this.button_Port.Text = "Change COM Port";
            this.button_Port.UseVisualStyleBackColor = true;
            this.button_Port.Click += new System.EventHandler(this.button2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "COM Port";
            // 
            // textBoxMessege
            // 
            this.textBoxMessege.Location = new System.Drawing.Point(12, 95);
            this.textBoxMessege.Name = "textBoxMessege";
            this.textBoxMessege.Size = new System.Drawing.Size(362, 20);
            this.textBoxMessege.TabIndex = 9;
            // 
            // labelMessege
            // 
            this.labelMessege.AutoSize = true;
            this.labelMessege.Location = new System.Drawing.Point(12, 79);
            this.labelMessege.Name = "labelMessege";
            this.labelMessege.Size = new System.Drawing.Size(84, 13);
            this.labelMessege.TabIndex = 10;
            this.labelMessege.Text = "Messege NMEA";
            // 
            // labelNumberSatelites
            // 
            this.labelNumberSatelites.AutoSize = true;
            this.labelNumberSatelites.Location = new System.Drawing.Point(200, 227);
            this.labelNumberSatelites.Name = "labelNumberSatelites";
            this.labelNumberSatelites.Size = new System.Drawing.Size(99, 13);
            this.labelNumberSatelites.TabIndex = 11;
            this.labelNumberSatelites.Text = "Number of Satelites";
            // 
            // textBoxNumerSatelites
            // 
            this.textBoxNumerSatelites.Location = new System.Drawing.Point(305, 224);
            this.textBoxNumerSatelites.Name = "textBoxNumerSatelites";
            this.textBoxNumerSatelites.Size = new System.Drawing.Size(32, 20);
            this.textBoxNumerSatelites.TabIndex = 13;
            // 
            // textBoxTime
            // 
            this.textBoxTime.Location = new System.Drawing.Point(71, 202);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(56, 20);
            this.textBoxTime.TabIndex = 14;
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Location = new System.Drawing.Point(31, 205);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(30, 13);
            this.labelTime.TabIndex = 15;
            this.labelTime.Text = "Time";
            // 
            // labelUTC
            // 
            this.labelUTC.AutoSize = true;
            this.labelUTC.Location = new System.Drawing.Point(133, 205);
            this.labelUTC.Name = "labelUTC";
            this.labelUTC.Size = new System.Drawing.Size(29, 13);
            this.labelUTC.TabIndex = 16;
            this.labelUTC.Text = "UTC";
            // 
            // labelPortStatus
            // 
            this.labelPortStatus.AutoSize = true;
            this.labelPortStatus.Location = new System.Drawing.Point(69, 29);
            this.labelPortStatus.Name = "labelPortStatus";
            this.labelPortStatus.Size = new System.Drawing.Size(25, 13);
            this.labelPortStatus.TabIndex = 17;
            this.labelPortStatus.Text = "port";
            // 
            // frmPP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 306);
            this.Controls.Add(this.labelPortStatus);
            this.Controls.Add(this.labelUTC);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.textBoxTime);
            this.Controls.Add(this.textBoxNumerSatelites);
            this.Controls.Add(this.labelNumberSatelites);
            this.Controls.Add(this.labelMessege);
            this.Controls.Add(this.textBoxMessege);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button_Port);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMapIt);
            this.Controls.Add(this.txtLat);
            this.Controls.Add(this.txtLong);
            this.Controls.Add(this.button1);
            this.Name = "frmPP";
            this.Text = "GPS Reader";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtLong;
        private System.Windows.Forms.TextBox txtLat;
        private System.Windows.Forms.Button btnMapIt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button_Port;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxMessege;
        private System.Windows.Forms.Label labelMessege;
        private System.Windows.Forms.Label labelNumberSatelites;
        private System.Windows.Forms.TextBox textBoxNumerSatelites;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Label labelUTC;
        private System.Windows.Forms.Label labelPortStatus;
    }
}